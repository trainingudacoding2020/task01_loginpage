import 'package:flutter/material.dart';
import './pageLogin.dart';

void main() => runApp(MaterialApp(
      title: "Flutter Login Page",
      home: LoginPage(),
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
        canvasColor: Colors.green.shade100,
      ),
    ));
